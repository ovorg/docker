#!/bin/sh

docker-compose exec ovorg_backend php artisan down
docker-compose exec laravel_worker php artisan queue:restart
docker-compose up -d --scale ovorg_backend=2 --scale laravel_worker=2

sleep 5

docker-compose up -d --scale ovorg_backend=1
docker-compose exec ovorg_backend php artisan up
