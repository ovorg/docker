#!/bin/sh

set -e

php artisan config:clear
php artisan clear-compiled
php artisan cache:clear

php artisan optimize
php artisan config:cache
php artisan route:cache
php artisan event:cache
php artisan view:cache

php artisan migrate

set -- php-fpm "$@"

exec "$@"
